terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.33.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "Terraform-Backend"
    storage_account_name = "terraform0807"
    container_name       = "terraform-tfstate"
    key                  = "qa.tfstate"
  }
}


provider "azurerm" {
  features {}
}