variable "rg_name_m" {
    description = "Name of the Resource_group"
    type = string
}
variable "rg_location_m" {
    description = "Location of the Resource_group"
    type = string
}