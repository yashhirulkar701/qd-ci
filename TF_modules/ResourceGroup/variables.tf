variable "rg_name" {
    description = "Name of the Resource_group"
    type = string
}

variable "rg_location" {
    description = "Location of the Resource_group"
    type = string
}
